package auditorium.array;

/**
 * Created by karenvardanyan on 9/25/16.
 */
public class ArrayDemo {

	public static void main(String[] args) {
		double [] numbers = new double[15];

        int size = numbers.length;
		for (int i = 1; i < size; i++) {
			numbers[i] = i + numbers[i-1];
		}
		numbers[0] = numbers[1] + numbers[size-1];

		ArrayUtil.print(numbers);
	}

}
