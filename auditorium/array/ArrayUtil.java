package auditorium.array;


import java.util.*;

/**
 * Created by karenvardanyan on 9/25/16.
 */
public class ArrayUtil {


	public static void getRandomIntArray(int length){
		getRandomIntArray( length, 100);
	}

	public static int[] getRandomIntArray(int length, int range){
		int [] array = new int[length];
		Random random = new Random();
		for (int i = 0; i < array.length; i++){
			array[i] = random.nextInt(range);
		}
		return array;
	}

	/**
	 * Sorts the array elements by odd and even number
	 * such that if first number is odd then second must be even number
	 * this means that it must be found the next even number and swap with the second element
	 * if the second element is odd. And so on for the next elements of the util
	 */
	public static void sortByOddAndEven(int[] a) {
		int stepsCount = a.length - 3;
		for (int i = 0; i <= stepsCount; i++) {
			int j = i + 1;
			while((a[i] + a[j]) % 2 == 0 ) {
				j++;
				if(j == a.length) return;
			}

			swap(a, i+1, j);
		}
	}


	public static int getMinimum(int[] array) {
		int min = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}


	public static int getMaximum(int[] array) {
		int max = array[0];
		int length = array.length;
		for (int i = 1; i < length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;

	}

	public static double getAverage (int[] array) {
		return getSum(array) / array.length;
	}


	/**
	 * Calculates the sum of specified <code>util</code>  elements
	 *
	 * @param array specified int util
	 * @return the sum of the specified util elements
	 */
	public static int getSum(int[] array) {
		int sum = 0;
		int length = array.length;
		for (int i = 0; i < length; i++) {
			sum += array[i];
		}
		return sum;
	}


	/**
	 * Creates the reversed to specified array new array and returns it.
	 *
	 * @param array an array corresponding to which must be created reversed array
	 * @return new created reversed array to the specified parameter "array"
	 */
	public static int[] reverse(int[] array) {
		int[] reversed = new int[array.length];
		int lastIndex = array.length -1;
		for (int i = 0; i < reversed.length; i++) {
			reversed[i] = array[lastIndex - i];
		}
	    return reversed;
	}

	/**
	 * Reverses the elements order of the specified array
	 *
	 * @param array an array: Elements order of wich must be reversed
	 */
	public static void reverseInSameArray(int[] array) {
		int halfLength = array.length/2;
		int lastIndex = array.length - 1;
		for (int i = 0; i < halfLength; i++) {
            swap(array, i, lastIndex - i);
		}

	}

	/**
	 * Swaps the elements at the specified positions in the specified array.
	 * (If the specified positions are equal, invoking this method leaves
	 * the array unchanged.)
	 *
	 * @param array The array in which to swap elements.
	 * @param i     the index of one element to be swapped.
	 * @param j     the index of the other element to be swapped.
	 * @throws IndexOutOfBoundsException if either i or j
	 *                                   is out of range (i < 0 || i >= array.length
	 *                                   || j < 0 || j >= array.length).
	 */
	public static void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	public static void print(int[] a) {
		print(a, " ");
	}

	public static void print(long[] a) {
		print(a, " ");
	}

	/**
	 * Prints all elements in an array by using specified delimiter between elements.
	 *
	 * @param a the util to print
	 */
	public static void print(int[] a, String delimiter) {
		if(a == null){
			System.out.println( a);
			return;
		}
		int length = a.length;
		for (int i = 0; i < length; i++) {
			System.out.print(delimiter + a[i]);
		}
		System.out.println();
	}

	/**
	 * Prints all elements in an array by using specified delimiter between elements.
	 *
	 * @param a the util to print
	 */
	public static void print(long[] a, String delimiter) {
		if(a == null){
			System.out.println( a);
			return;
		}
		int length = a.length;
		for (int i = 0; i < length; i++) {
			System.out.print(delimiter + a[i]);
		}
		System.out.println();
	}

	public static void print(double[] a) {
		print(a, " ");
	}
	/**
	 * Prints all elements in an array by using specified delimiter between elements.
	 *
	 * @param a the util to print
	 */
	public static void print(double[] a, String delimiter) {
		int length = a.length;
		for (int i = 0; i < length; i++) {
			System.out.print(delimiter + a[i]);
		}
		System.out.println();
	}

	public static void main(String[] args) {
		int[] a = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2};
		int[] b = a;
		a[0] = 1000;
		System.out.println("b[0] = " + b[0]);
		System.out.println(a);
		System.out.println(b);

		print(a);
		sortByOddAndEven(a);
		print(a);
	}




}
