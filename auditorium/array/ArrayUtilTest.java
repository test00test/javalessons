package auditorium.array;

/**
 * Created by karenvardanyan on 10/1/16.
 */
public class ArrayUtilTest {


	public static void main(String[] args) {

//		int[] b = a;
//		a[0] = 1000;
//		System.out.println("b[0] = " + b[0]);
//		System.out.println(a);
//		System.out.println(b);
		reverseInSameArrayTest();

	}


	static void sortByOddAndEvenTest() {
		int[] a = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2};
		ArrayUtil.print(a);
		ArrayUtil.sortByOddAndEven(a);
		ArrayUtil.print(a);
	}

	static void reverseTest() {
		int[] a = ArrayUtil.getRandomIntArray(5, 100);

		ArrayUtil.print(a);
		a = ArrayUtil.reverse(a);
		ArrayUtil.print(a);
		System.out.println(a);

	}

	static void reverseInSameArrayTest() {
		int[] a = ArrayUtil.getRandomIntArray(5, 100);

		ArrayUtil.print(a);
		ArrayUtil.reverseInSameArray(a);
		ArrayUtil.print(a);

	}



}
