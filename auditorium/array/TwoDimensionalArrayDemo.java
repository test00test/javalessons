package auditorium.array;

/**
 * Created by karenvardanyan on 10/2/16.
 */
public class TwoDimensionalArrayDemo {

	public static void main(String[] args) {

		long[][] a = null;
//		print(a);

		a = new long[15][];
		print(a);

		for (int i = 0, currentValue = 0; i < a.length; i++) {
			a[i] = new long[i + 1];
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = currentValue++;
			}
		}
		print(a);

		a = new long[][]{
			{1},
			{1, 2},
			{}
		};
		print(a);
	}

	static void print(long[][] a){
		if(a == null){
			System.out.println(a);
			return;
		}

		for (int i = 0; i < a.length; i++) {
			ArrayUtil.print(a[i]);
		}
	}
}
