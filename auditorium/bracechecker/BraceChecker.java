package auditorium.bracechecker;

public class BraceChecker {

	private String resultMessage;
	private Stack stack;

	public BraceChecker() {
		stack = new Stack();
	}

	public void parse(String inputText) {
		if(inputText == null || inputText.length() == 0){
			return;
		}
		reset();

		int length = inputText.length();
		int i = 0;
		char stackLastElement = 0;
		char ch = 0;
		lab: for (; i < length; i++) {
			ch = inputText.charAt(i);

			switch (ch) {
				case '{':
				case '(':
				case '[':
					stack.push(ch);
					break;
				case '}':
					stackLastElement = stack.pop();
					if(stackLastElement != '{'){
						break lab;
					}
					break ;
				case ')':
					stackLastElement = stack.pop();
					if(stackLastElement != '('){
						break lab;
					}
					break ;
				case ']':
					stackLastElement = stack.pop();
					if(stackLastElement != '['){
						break lab;
					}
					break ;
			}
		}

		if(i < length){
			if(stackLastElement == 0){
				resultMessage = "Error: closed '" + ch + "' but not opened!";
			} else {
				resultMessage = "Error: opened '" + stackLastElement + "' but closed '" + ch + "'!";
			}

		} else if(!stack.isEmpty()){
			resultMessage = "Error: opened '" + stack.pop() + "' but not closed !";
		}
	}


	public String getResultMessage() {
		return resultMessage;
	}

	private void reset(){
		resultMessage = "No Error";
		stack.clear();
	}



}
