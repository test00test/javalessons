package auditorium.bracechecker;

/**
 * Created by karenvardanyan on 10/16/16.
 */
public class BraceCheckerTest {
	static BraceChecker braceChecker = new BraceChecker();

	public static void main(String[] args) {
		BraceChecker braceChecker = new BraceChecker();
		parserFirstCase();


	}

	/**
	 * opened but not closed
	 */
	private static void parserFirstCase (){
		braceChecker.parse("{{ss(s)");
		String expectedMessage = "Error: opened '{' but not closed !";
		if(!expectedMessage.equals(braceChecker.getResultMessage())){
			System.err.println("parse method test failure: opened but not closed case for '{'");
		}

		braceChecker.parse("(");
		expectedMessage = "Error: opened '(' but not closed !";
		if(!expectedMessage.equals(braceChecker.getResultMessage())){
			System.err.println("parse method test failure: opened but not closed case for '{'");
		}

		braceChecker.parse("{{ss( ) [");
		expectedMessage = "Error: opened '[' but not closed !";
		if(!expectedMessage.equals(braceChecker.getResultMessage())){
			System.err.println("parse method test failure: opened but not closed case for '['");
		}
	}

	/**
	 * closed but not opened
	 */
	private static void parserSecondCase (){

	}

	/**
	 * closed incompatible bracket
	 */
	private static void parserThirdCase (){

	}


}
