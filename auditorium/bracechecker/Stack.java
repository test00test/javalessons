package auditorium.bracechecker;


public class Stack {
	static final int DEFAULT_SIZE = 16;
	char[] data;
	int tos;

	public Stack() {
		this(DEFAULT_SIZE);
	}

	public Stack(int size) {
		data = new char[size];
		this.tos = -1;
	}

	public void push(char value){
		if(tos == data.length - 1 ){
			System.err.print("Stack is full!");
			return;
		}

		data[++tos] = value;
	}

	public char pop(){
		if(tos < 0) {
			System.err.print("Stack is empty!");
			return 0;
		}

		return data[tos--];
	}


	public void clear(){
		tos = -1;
	}


	public boolean isEmpty(){
		return tos == -1;
	}

}
