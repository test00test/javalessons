package auditorium.classintro;

/**
 * Created by karenvardanyan on 10/9/16.
 */
public class Rectangle {
	public static final int DEFAULT_X      = 50;
	public static final int DEFAULT_Y      = 50;
	public static final int DEFAULT_WIDTH  = 150;
	public static final int DEFAULT_HEIGHT = 100;

	private int x = 111;
	private int y;
	private int width ;
	private int height;

	public Rectangle() {
		this(DEFAULT_X, DEFAULT_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);

	}


	public Rectangle(int x, int y) {
		this(x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	Rectangle(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public int getX() {

		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		if(isNotValidateParameter(width, "width")){
			return;
		}
		this.width = width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		if(isNotValidateParameter(height, "height")){
			return;
		}
		this.height = height;
	}

	private boolean isNotValidateParameter(int parameter, String parameterName){
		if(parameter < 0){
			System.err.println("The " + parameterName  +  " of recatangle can not be negative: " + parameter);
			return true;
		}
		return false;
	}

	public int getSquare(){
		return width * height;
	}

	@Override
	public String toString() {
		return "Rectangle{" +
			"x=" + x +
			", y=" + y +
			", width=" + width +
			", height=" + height +
			'}';
	}
}
