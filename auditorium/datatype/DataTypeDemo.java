package auditorium.datatype;

import auditorium.array.*;
import java.util.*;


/**
 * Created by karenvardanyan on 9/17/16.
 */
public class DataTypeDemo {
	int g = 100;

	public static void main(String[] args) {

		byte a = -123;
		short shrt = a;
		char ch = (char)a;
//		System.out.println("ch = " + ch);
		System.out.println( (short)'\uFFFF');
//		ch = (short)(ch + 1);

//		printASCCIChars();
//		showCharsUsage();
//		showNumbersUsage();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter the length of an array:");
		int arrayLength = scanner.nextInt();
		int[] array = new int[arrayLength];
		for (int i = 0; i < arrayLength; i++) {
			System.out.print("Please enter the next element: \naraay[" + i + "] = ");
			array[i] = scanner.nextInt();
		}

		ArrayUtil.print(array);

//		int revers = getReversedNumber(inputNumber);
//		System.out.println(inputNumber + " : " + revers);

	}

	static void showBooleanUsage(){
		int a = 2;
		int b = 3;
		boolean bool = true;  // 1 byte
//		boolean bool2 = 2 == 2;  // won't compile

		bool = a > b;
		if(2 < 3 ) {
			System.out.println();
		}
	}

	static void showCharsUsage () {
		char ch = 'T';
		System.out.println(ch + 1);
		System.out.println( "ch = " + ch);
		ch = '\u001f';
		System.out.println( "ch = " + ch);
		ch = 65000;
		System.out.println( "ch = " + ch);

	}

	static void showNumbersUsage () {
		System.out.println("****** IN showNumbersUsage method *****");
		short b = 1;                     // 1 byte
		short a = 1;                     // 1 byte
		b = (short)(b + a);
		short sh = -32000;                // 2 byte

		int n;                            // 4 byte

		long lng = 1;                       // 8 byte
//		lng = 1.0;   // won't compile

		// rational numbers
		float someFloat = 1.13f;      // 4 byte
		double someDouble = 4;        // 8 byte

		showFloatingPointDefects();

		if (b == a) {
			System.out.println("Say yes");
		} else {
			System.out.println("Say No");
		}

	}

	static void showFloatingPointDefects() {
		double i =  0.1;
//		System.out.println("i = " + i);

		for (; i <= 1; i = i + 0.1) {
			System.out.println(i);

		}
		System.out.println(i);
	}

	static void printASCCICharsOld() {
		for (int i = 32; i < 128; i++) {
			System.out.println((char) i + " : " + i);
		}
	}

	static void printASCCIChars() {

		for (char i = 32; i < 128; i = (char)(i + 1)) {
			System.out.println(i + " " + (byte)i);
		}
	}

	static int getReversedNumber(int inputNumber) {
		int temp = inputNumber < 0 ? -inputNumber : inputNumber;
		int result = getReversedNumber0 (temp);
		return  inputNumber < 0 ? -result : result;
	}

	private static int getReversedNumber0 (int inputNumber) {
		int result = 0;
		while (inputNumber != 0) {
			result = result * 10 + inputNumber % 10;
			inputNumber = inputNumber / 10;
		}
		return  result;
	}
}


