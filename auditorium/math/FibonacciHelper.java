package auditorium.math;

import java.util.*;

/**
 * Created by zhora on 10/8/16.
 */
public class FibonacciHelper {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		String isRunningStr  = "";
		boolean isRunning = false;

		do {
			System.out.println("Please enter a number to print whether it is fibonacci number or not:\n ");
			int number = scanner.nextInt();
			int index = indexOfFibonacciNumber(number);
			System.out.println(index > -1);

			System.out.println("Print 'Y' if you want to continue: otherwise 'N'");
			 isRunning = scanner.nextBoolean();
		} while (isRunning ) ;


	}

	public static int getFibonacciNumber(int index) {
		if (index < 0) {
			System.err.print("The index can not be negative: " + index);
			return -1;
		}
		if (index < 2) return 1;
		int previous = 1;
		int current = 1;
		for (int i = 2; i <= index; i++) {
			int tmp = current;
			current = current + previous;
			previous = tmp;
		}
		return current;
	}

	public static int[] getFibonacciNumbers(int count) {
		// TODO add validation for count parameter, but later
		switch(count){
			case 1:
				return new int[]{1};
			case 2:
				return new int[]{1, 1};
		}

		int array[] = new int[count];
		array[0] = 1;
		array[1] = 1;
		for (int i = 2; i < array.length; i++) {
			array[i] = array[i - 2] + array[i - 1];

		}
		return array;
	}

	public static int indexOfFibonacciNumber(int number) {
		if (number == 1) {
			return 0;
		}
		for (int i = 2, current = 1; current < number; i++) {
			current = getFibonacciNumber(i);
			if(current == number){
				return i;
			}
		}
		return -1;
	}
}
