package auditorium.math;

import auditorium.operator.*;

import java.util.*;

public class MathUtilTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        powTest(scanner);
    }

    public static void powTest(Scanner scanner) {
        System.out.println("Please input the base of pow:");
        int a = scanner.nextInt();
        System.out.println("Please input the exponent of pow:");
        int n = scanner.nextInt();

        long res = MathUtil.pow(a, n);

        System.out.println("((int)res) == Integer.MIN_VALUE : " + (((int)res) == Integer.MIN_VALUE));
        OperatorsDemo.printAsBinary((int)(res));
        res = res -1;
        System.out.println("res -1 == Integer.MAX_VALUE" + (res == Integer.MAX_VALUE));

        OperatorsDemo.printAsBinary((int)(res));
    }
}
