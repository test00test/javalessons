package auditorium.operator;

import java.util.*;

/**
 * Created by karenvardanyan on 10/8/16.
 */
public class ControlStatments {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Please enter the grade: ");
		int grade = scanner.nextByte();

		System.out.println(getGradeName(grade));

		Random random = new Random();
		int die = random.nextInt(6) + 1;
		switch (die) {
			case 1:
				System.out.println("TODO: call print 1 method");
				break;
			case 2:
				System.out.println("TODO: call print 2 method");
				break;
			case 3:
				System.out.println("TODO: call print 3 method");
				break;
			case 4:
				System.out.println("TODO: call print 4 method");
				break;
			case 5:
				System.out.println("TODO: call print 5 method");
				break;
			default:
				System.out.println("TODO: call print 6 method");
		}


	}



	public static String getGradeName(int grade ){
		String gradeName = "None";
		if(grade < 1 || grade > 100){
			return gradeName;
		}
		if(grade < 40){
			gradeName = "Bad";
		} else if (grade < 60){
			gradeName = "Not bad";
		}else if (grade < 80){
			gradeName = "Good";
		} else {
			gradeName = "Excellent";
		}
		return gradeName;
	}
}
