package auditorium.operator;

public class OperatorsDemo {

	public static void main(String[] args) {
		int b = -123;
		//System.out.println((b >> 7 & 1));
		printAsBinary(-1);

	}

	public static void printAsBinary(int number){
		for (int i = 31; i >= 0; i--) {
			System.out.print((number >> i & 1));
			if(i % 8 == 0) System.out.print((' '));
		}
		System.out.println();
	}
}
